package br.com.enums;

public enum StatusCadastroEnum {
    ATIVO,
    ACEITO,
    RECUSADO,
    EMAVALIACAO
}
