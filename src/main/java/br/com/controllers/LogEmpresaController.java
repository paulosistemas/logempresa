package br.com.controllers;

import br.com.models.Empresa;
import br.com.services.LogEmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/Empresa/Log")
public class LogEmpresaController {

    @Autowired
    LogEmpresaService logEmpresaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void gravarLog(@RequestBody Empresa empresa) throws IOException {

        logEmpresaService.gravarLog(empresa);

    }

}
