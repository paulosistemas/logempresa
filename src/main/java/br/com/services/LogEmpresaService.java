package br.com.services;

import br.com.models.Empresa;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@Service
public class LogEmpresaService {

    public void gravarLog(Empresa empresa) throws IOException {

        String path = "/home/a2w/Documentos/CartoesMicroservices/Kafka/LogValidaEmpresa/logEmpresa.csv";

        // gravar um arquivo sequencial, de maneira simples e no final do arquivo (parâmetro append)
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path, true))){
            bufferedWriter.write(empresa.toString());
            bufferedWriter.newLine();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
