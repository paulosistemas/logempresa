package br;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogEmpresaApplication {

    public static void main(String[] args){
        SpringApplication.run(LogEmpresaApplication.class, args);
    }
}
